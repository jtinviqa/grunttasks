'use strict';

module.exports = function(grunt) {
    require('time-grunt')(grunt);

    //NOTE: the 'DEFAULT' variables are only used if parameters are not passed on the commandline to grunt.
    var DEFAULT_WEBDAV_SERVER = 'Your Demandware server hostname goes here',
        DEFAULT_WEBDAV_USERNAME = 'Your BM username goes here',
        DEFAULT_WEBDAV_PASSWORD = 'Your BM password goes here',
        webDavServer = grunt.option('WEBDAV_SERVER') || DEFAULT_WEBDAV_SERVER,
        webDavUsername = grunt.option('WEBDAV_USERNAME') || DEFAULT_WEBDAV_USERNAME,
        webDavPassword = grunt.option('WEBDAV_PASSWORD') || DEFAULT_WEBDAV_PASSWORD;

    grunt.initConfig({              
        resetpassword: {
            options: {              
                webdav_server: webDavServer,
                webdav_username: webDavUsername,
                webdav_password: webDavPassword               
            }
        }
    }); 
        
    grunt.loadTasks("./tasks");
    grunt.registerTask('default', ['resetpassword']);
};